package main

import (
	"runtime"
)

const bytesPerPage = 4096

type ProcMon struct {
	Name string
	Pid int64
	StartTime int64
	CpuUsage int   // Percent
	MemUsage int64 // Kibibytes
	prevProcStat *ProcStat
	prevProcPidStat *ProcPidStat
}

func NewProcMon(procStat *ProcStat, procPidStat *ProcPidStat) ProcMon {
	return ProcMon{
		Name: procPidStat.Comm,
		Pid: procPidStat.Pid,
		StartTime: procPidStat.StartTime,
		CpuUsage: 0,
		MemUsage: 0,
		prevProcStat: procStat,
		prevProcPidStat: procPidStat,
	}
}

func (mon *ProcMon) Update(procStat *ProcStat, procPidStat *ProcPidStat) {
	totalDelta := procStat.Total - mon.prevProcStat.Total
	ustimeDelta :=
		(procPidStat.Utime + procPidStat.Stime) -
		(mon.prevProcPidStat.Utime + mon.prevProcPidStat.Stime)
	idleDelta := totalDelta - ustimeDelta
	idleFrac := float64(idleDelta) / float64(totalDelta)

	numCpu := float64(runtime.NumCPU())
	usage := (1 - idleFrac) * numCpu
	mon.CpuUsage = int(usage * 100)
	mon.MemUsage = (procPidStat.RSS * bytesPerPage) / 1024

	mon.prevProcStat = procStat
	mon.prevProcPidStat = procPidStat
}

type SysMon struct {
	CpuUsage int   // Percent
	MemUsage int64 // Kibibytes
	prevProcStat *ProcStat
}

func NewSysMon(procStat *ProcStat) SysMon {
	return SysMon{
		CpuUsage: 0,
		MemUsage: 0,
		prevProcStat: procStat,
	}
}

func (mon *SysMon) Update(procStat *ProcStat, procMeminfo *ProcMeminfo) {
	idleDelta := procStat.Idle - mon.prevProcStat.Idle
	totalDelta := procStat.Total - mon.prevProcStat.Total
	idleFrac := float64(idleDelta) / float64(totalDelta)

	numCpu := float64(runtime.NumCPU())
	usage := (1 - idleFrac) * numCpu
	mon.CpuUsage = int(usage * 100)
	mon.MemUsage = procMeminfo.Total - procMeminfo.Available

	mon.prevProcStat = procStat
}
