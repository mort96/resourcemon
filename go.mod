module gitlab.com/mort96/resourcemon

go 1.17

require gitlab.com/mort96/gograph v0.0.0-20210226093802-dfe1da3eef23

require (
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/rakyll/statik v0.1.7 // indirect
)
