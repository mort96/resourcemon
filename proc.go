package main

import (
	"os"
	"bufio"
	"strings"
	"strconv"
)

type ProcPidStat struct {
	Pid int64
	Comm string
	Utime int64
	Stime int64
	RSS int64
	StartTime int64
}

func ReadProcPidStat(pid int64) (*ProcPidStat, error) {
	file, err := os.Open("/proc/"+strconv.FormatInt(pid, 10)+"/stat")
	if err != nil { return nil, err }
	defer file.Close()
	r := bufio.NewReader(file)

	line, err := r.ReadString('\n')
	if err != nil { return nil, err }

	commParts := strings.Split(line, ") ")
	comm := strings.Split(commParts[0], " (")[1]

	fields := strings.Fields(commParts[1])

	utime, err := strconv.ParseInt(fields[11], 10, 64)
	if err != nil { return nil, err }
	stime, err := strconv.ParseInt(fields[12], 10, 64)
	if err != nil { return nil, err }
	rss, err := strconv.ParseInt(fields[21], 10, 64)
	if err != nil { return nil, err }
	starttime, err := strconv.ParseInt(fields[19], 10, 64)
	if err != nil { return nil, err }

	return &ProcPidStat{
		Pid: pid,
		Comm: comm,
		Utime: utime,
		Stime: stime,
		RSS: rss,
		StartTime: starttime,
	}, nil
}

type ProcStat struct {
	Idle int64
	Total int64
}

func ReadProcStat() (*ProcStat, error) {
	file, err := os.Open("/proc/stat")
	if err != nil { return nil, err }
	defer file.Close()
	r := bufio.NewReader(file)

	line, err := r.ReadString('\n')
	if err != nil { return nil, err }

	fields := strings.Fields(line)

	idle, err := strconv.ParseInt(fields[4], 10, 64)
	if err != nil { return nil, err }

	var total int64 = 0
	for _, val := range fields[1:] {
		num, err := strconv.ParseInt(val, 10, 64)
		if err != nil { return nil, err }
		total += num
	}

	return &ProcStat{
		Idle: idle,
		Total: total,
	}, nil
}

type ProcMeminfo struct {
	Total int64
	Free int64
	Available int64
}

func readMeminfoLine(r *bufio.Reader) (int64, error) {
	line, err := r.ReadString('\n')
	if err != nil { return 0, err }
	fields := strings.Fields(line)
	return strconv.ParseInt(fields[1], 10, 64)
}

func ReadProcMeminfo() (*ProcMeminfo, error) {
	file, err := os.Open("/proc/meminfo")
	if err != nil { return nil, err }
	defer file.Close()
	r := bufio.NewReader(file)

	memTotal, err := readMeminfoLine(r)
	if err != nil { return nil, err }
	memFree, err := readMeminfoLine(r)
	if err != nil { return nil, err }
	memAvailable, err := readMeminfoLine(r)
	if err != nil { return nil, err }

	return &ProcMeminfo{
		Total: memTotal,
		Free: memFree,
		Available: memAvailable,
	}, nil
}
