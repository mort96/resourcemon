package main

import (
	"io/ioutil"
	"os"
	"net"
	"time"
	"strconv"
	"sort"
	"flag"
	"gitlab.com/mort96/gograph"
)

type ResourceMonitor struct {
	sysMon SysMon
	numProcs int
	procMons map[int64]*ProcMon
}

func NewResourceMonitor(numProcs int) ResourceMonitor {
	procStat, err := ReadProcStat()
	if err != nil { panic(err) }

	return ResourceMonitor{
		sysMon: NewSysMon(procStat),
		numProcs: numProcs,
		procMons: make(map[int64]*ProcMon),
	}
}

func (mon *ResourceMonitor) Update(xval string) gograph.GraphSetUpdate {
	procStat, err := ReadProcStat()
	if err != nil { panic(err) }
	procMeminfo, err := ReadProcMeminfo()
	if err != nil { panic(err) }

	mon.sysMon.Update(procStat, procMeminfo)

	files, err := ioutil.ReadDir("/proc")
	if err != nil { panic(err) }

	var procPidStats []*ProcPidStat

	for _, entry := range files {
		if !entry.IsDir() { continue }
		pid, err := strconv.ParseInt(entry.Name(), 10, 64)
		if err != nil { continue }
		procPidStat, err := ReadProcPidStat(pid)
		if err != nil { continue }

		procPidStats = append(procPidStats, procPidStat)
	}

	sort.Slice(procPidStats, func(i, j int) bool {
		return procPidStats[i].RSS > procPidStats[j].RSS
	})

	procNum := mon.numProcs
	if procNum > len(procPidStats) {
		procNum = len(procPidStats)
	}

	memGraphUpdate := gograph.GraphUpdate{
		Name: "Memory (MiB)",
		XVal: xval,
		Lines: make([]gograph.GraphLineUpdate, procNum + 1),
	}
	memGraphUpdate.Lines[0] = gograph.GraphLineUpdate{
		Name: "Total",
		YVal: float64(mon.sysMon.MemUsage / 1024.0),
	}

	cpuGraphUpdate := gograph.GraphUpdate{
		Name: "CPU (%)",
		XVal: xval,
		Lines: make([]gograph.GraphLineUpdate, procNum + 1),
	}
	cpuGraphUpdate.Lines[0] = gograph.GraphLineUpdate{
		Name: "Total",
		YVal: float64(mon.sysMon.CpuUsage),
	}

	for i, procPidStat := range procPidStats[:procNum] {
		procMon, ok := mon.procMons[procPidStat.Pid]
		if ok {
			procMon.Update(procStat, procPidStat)
		} else {
			pm := NewProcMon(procStat, procPidStat)
			procMon = &pm
			mon.procMons[procPidStat.Pid] = procMon
			continue
		}

		name :=
			"("+strconv.FormatInt(procMon.Pid, 10)+") "+
			procMon.Name+
			"\x1b"+strconv.FormatInt(procMon.StartTime, 10)

		memGraphUpdate.Lines[i+1] = gograph.GraphLineUpdate{
			Name: name,
			YVal: float64(procMon.MemUsage) / 1024.0,
		}

		cpuGraphUpdate.Lines[i+1] = gograph.GraphLineUpdate{
			Name: name,
			YVal: float64(procMon.CpuUsage),
		}
	}

	return gograph.GraphSetUpdate{
		Graphs: []gograph.GraphUpdate{memGraphUpdate, cpuGraphUpdate},
	}
}

func main() {
	addr := flag.String("addr", ":8080", "HTTP listen address")
	numProcs := flag.Int("num", 40, "Track the top <num> memory intensive processes")
	logFile := flag.String("log", "", "The log file, defaults to creating a new temp file")
	disableLog := flag.Bool("no-log", false, "Disable logging altogether")
	serve := flag.String("serve", "", "Serve a log file instead of logging more data")
	flag.Parse()

	// Print addresses, for convenience
	ifaceAddrs, err := net.InterfaceAddrs()
	if err != nil { panic(err) }
	for _, addr := range ifaceAddrs {
		ip, ok := addr.(*net.IPNet)
		if ok && !ip.IP.IsLoopback() && ip.IP.To4() != nil {
			str := ip.IP.String()
			println("IP Address: "+str)
		}
	}

	var graph *gograph.GoGraph

	// Only serve if the -serve flag was provided
	if *serve != "" {
		log, err := os.Open(*serve)
		if err != nil { panic(err) }

		graph, err = gograph.NewGoGraph(log)
		if err != nil { panic(err) }
		defer graph.Close()

		println("Serving", log.Name())
		graph.Serve(*addr)
		return
	}

	// Create/open a log file if necessary
	if !*disableLog {
		var log *os.File = nil
		var err error
		if *logFile != "" {
			log, err = os.OpenFile(*logFile, os.O_APPEND | os.O_CREATE | os.O_RDWR, 0644)
			if err != nil { panic(err) }
			defer log.Close()
			println("Writing logs to", log.Name())
		}
		graph, err = gograph.NewGoGraph(log)
		if err != nil { panic(err) }
	} else {
		var err error
		graph, err = gograph.NewGoGraph(nil)
		if err != nil { panic(err) }
	}

	defer graph.Close()
	go graph.Run(*addr)
	println("Listening on "+*addr)

	monitor := NewResourceMonitor(*numProcs)
	time.Sleep(1 * time.Second)
	monitor.Update("")

	for {
		time.Sleep(1 * time.Second)
		xval := time.Now().Format(time.RFC3339)
		update := monitor.Update(xval)
		graph.Update(update)
	}
}
